#include "util.h"
#include "nfa.h"

#include <iostream>
#include <unordered_map>
#include <vector>

int main() {
    nfa::transition_table nfa_transition_table;

    std::string sigma;
    std::cout << "Enter the possible input alphabets without space\n";
    std::cin >> sigma;

    std::cout << "Sigma: " << sigma << '\n';

    std::vector<std::string> states;
    std::vector<std::string> final_states;

    while (true) {
        std::string state;
        std::cout << "Enter the name of the state of -1 if no more states remain\n";
        std::cin >> state;

        if (state == "-1") {
            break;
        }

        states.push_back(state);

        std::cout << "Is " << state << " final? 0 or 1\n";
        bool final;
        std::cin >> final;

        if (final) {
            final_states.push_back(state);
        }
    }

    std::sort(states.begin(),  states.end());
    std::sort(final_states.begin(),  final_states.end());

    std::cout << "states: ";
    for (std::string &s : states) {
        std::cout << s << ' ';
    }

    std::cout << '\n';

    std::cout << "final states: ";
    for (std::string &s : final_states) {
        std::cout << s << ' ';
    }

    std::cout << '\n';

    std::cout << "Enter the initial state\n";
    std::string initial_state;
    std::cin >> initial_state;

    std::cout << "Initial state: " << initial_state << '\n';

    std::cin.ignore();

    for (std::string &state: states) {
        for (nfa::Alphabet alphabet : sigma) {
            std::cout << "Where can " << state << " go for input " << alphabet << '\n';
            std::cout << "Press enter for no states\n";

            std::string line;
            std::getline(std::cin, line);

            std::vector<std::string> lines_after_splitting = split(line);
            std::sort(lines_after_splitting.begin(),  lines_after_splitting.end());
            nfa_transition_table[state][alphabet] = lines_after_splitting;
        }
    }

    nfa::nfa nfa_to_convert(nfa_transition_table, sigma, states, initial_state, final_states);
    nfa_to_convert.pretty_print();

    /*std::cout << "\t|";
    for (nfa::Alphabet alphabet : sigma) {
        std::cout << alphabet << "\t|\t";
    }
    std::cout << '\n';

    for (std::string &state : states) {
        std::cout << state << "\t|";
        for (nfa::Alphabet alphabet : sigma) {
             std::cout << join(nfa_transition_table[state][alphabet]) << "\t|";
        }
        std::cout << '\n';
    }*/
}
