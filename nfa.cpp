//
// Created by hemil on 01/05/22.
//

#include "nfa.h"
#include "util.h"

#include <utility>
#include <iomanip>
#include <iostream>
#include <algorithm>

namespace nfa {
    nfa::nfa(
            transition_table delta_,
            std::string sigma_,
            std::vector<std::string> states_,
            std::string initial_state_,
            std::vector<std::string> final_states_
            )
    : delta(std::move(delta_)),
      sigma(std::move(sigma_)),
      states(std::move(states_)),
      initial_state(std::move(initial_state_)),
      final_states(std::move(final_states_))
    {
        if (delta.size() != states.size()) {
            throw std::runtime_error("Size of delta and states should be same");
        }

        for (auto&[state, transitions] : delta) {
            if (transitions.size() != sigma.size()) {
                std::stringstream ss;
                ss << "Transitions for state " << state << " has size: "
                   << transitions.size() << " which is different "
                   << "from size of sigma which is "
                   << sigma.size();

                throw std::runtime_error(ss.str());
            }
        }
    }

    void nfa::pretty_print() const {
        if (delta.empty()) {
            std::cout << "Empty nfa\n";
            return;
        }
        
        std::vector<size_t> possible_width;

        for (auto&[state, transitions] : delta) {
            possible_width.push_back(state.size());

            for (auto& [alphabet, states] : transitions) {
                int width = 0;
                for (const std::string& s : states) {
                    // 1 extra for space
                    width += s.size() + 1;
                }

                // 2 for either arrow, 2 curly bracket
                width += 4;
                possible_width.push_back(width);
            }
        }

        /// One to adjust for the | character
        const int width = *std::max_element(possible_width.begin(), possible_width.end()) + 1;

        std::cout << "Width: " << width << '\n';

        std::cout << std::setw(width) << "";
        
        for (char input : sigma) {
            std::cout << std::setw(width) << input << '|';
        }

        std::cout << '\n';

        std::cout.fill('-');
        std::cout << std::setw((width + 2) * (sigma.size() + 1)) << "";
        std::cout.fill(' ');

        std::cout << '\n';

        for (const std::string& state : states) {
            std::string state_to_display = state;

            if (contains(final_states, state)) {
                state_to_display = "(" + state_to_display + ")";
            }

            if (state == initial_state) {
                state_to_display = "->" + state_to_display;
            }

            std::cout << std::setw(width) << state_to_display << "|";

            for (char input : sigma) {
                std::cout << std::setw(width) << join(delta.at(state).at(input)) << "|";
            }

            std::cout << '\n';
        }

        std::cout.fill('-');
        std::cout << std::setw((width + 2) * (sigma.size() + 1)) << "";
        std::cout.fill(' ');
    }
}
