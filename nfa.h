//
// Created by hemil on 01/05/22.
//

#ifndef NFA_TO_DFA_NFA_H
#define NFA_TO_DFA_NFA_H

#include <unordered_map>
#include <vector>

namespace nfa {
    using State = std::string;
    using Alphabet = char;

    using transition_table = std::unordered_map<
            State,
            std::unordered_map<
                    Alphabet,
                    std::vector<std::string>
            >
    >;



    class nfa {
        const transition_table delta;
        const std::string sigma;
        const std::vector<std::string> states;
        const std::string initial_state;
        const std::vector<std::string> final_states;
    public:
        explicit nfa(
                transition_table t,
                std::string sigma,
                std::vector<std::string> states,
                std::string initial_state,
                std::vector<std::string> final_states
                );

        void pretty_print() const;
    };
}

#endif //NFA_TO_DFA_NFA_H
