//
// Created by hemil on 01/05/22.
//

#ifndef NFA_TO_DFA_UTIL_H
#define NFA_TO_DFA_UTIL_H

#include <vector>
#include <string>
#include <sstream>
#include <algorithm>

inline std::vector<std::string> split(const std::string& s, const std::string& delimiter = " ")
{
    int start = 0;
    int end = s.find(delimiter);

    std::vector<std::string> result;

    while (end != -1) {
        result.push_back(s.substr(start, end - start));
        start = end + delimiter.size();
        end = s.find(delimiter, start);
    }
    std::string temp = s.substr(start, end - start);
    if (!temp.empty()) {
        result.push_back(temp);
    }

    return result;
}

inline std::string join(const std::vector<std::string>& strings, const std::string& delimiter = " ") {
    std::stringstream ss;

    for (const std::string& s : strings) {
        ss << s << delimiter;
    }

    return ss.str();
}

inline bool contains(std::vector<std::string> vec, std::string val) {
    return std::find(vec.begin(), vec.end(), val) != vec.end();
}

#endif //NFA_TO_DFA_UTIL_H
